import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient ) { }

  getNewRealeases() {
    return this.getQuery('browse/new-releases')
              .pipe( map( data => data['albums'].items ));
  }

  getArtists( termino: string ) {
    return this.getQuery(`search?q=${ termino }&type=artist`)
              .pipe( map( data =>  data['artists'].items ));
  }
  getArtist( artistaId: string ) {
    return this.getQuery(`artists/${artistaId}`);
  }
  getArtistTopTracks( artistaId: string ) {
    return this.getQuery(`artists/${artistaId}/top-tracks?country=ES`)
                .pipe( map( data => data['tracks']));
  }  
  getQuery( query: string){
    const headers = new HttpHeaders({
      'Authorization': 'Bearer QDj45ycNFFoa63UU61DpMIFb1IM6xPlThRym4VcyIDLma0vLWU2HR6YnPameWstjQMmupoYqT5nH4X5-pqboVzjtUHDqw9VB4iOegvUSiOH6pbzc1_qz69ezzs6eOXn8jMseTaj7FJr1S63TGL-K39t8el8Nds'
    });
    const URL = `https://api.spotify.com/v1/${ query }`;

    return this.http.get(URL, { headers });
  }
}
