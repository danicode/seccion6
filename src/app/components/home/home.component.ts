import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  loading: boolean;
  error: boolean;
  mError: string;
  nuevasCanciones: any[] = [];
  constructor(private spotifyService: SpotifyService) {
    this.loading = true;
    this.spotifyService.getNewRealeases().subscribe( (data: any) => {
      console.log(data);
      this.nuevasCanciones = data;
      this.loading = false;
    }, (error: any) => {
        this.loading = false;
        this.mError = error.error.error.message;
        this.error = true;
    });

  }

  ngOnInit(): void {
  }

}
