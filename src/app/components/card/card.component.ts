import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() public canciones: any[] = [];
  constructor( private router: Router ) { }

  ngOnInit(): void {
  }

  verArtista( cancion ) {
    let artistaId;
    if ( cancion.type === 'artist' ) {
      artistaId = cancion.id;
    } else {
      artistaId = cancion.artists[0].id;
    }
    this.router.navigate(['/artist', artistaId]);
  }

}
