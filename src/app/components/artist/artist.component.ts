import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent  {
  artista: any = {};
  tracks: any[] = [];
  loading: boolean;
  constructor( private activatedRoute: ActivatedRoute, 
               private spotifyService: SpotifyService ) {
    this.loading = true;
    this.activatedRoute.params.subscribe( params => {
      this.getArtist(params.id);
      this.getArtistTopTracks(params.id);
    });
    // this.loading = false;
  }

  getArtist( artistaId: string ) {
    this.spotifyService.getArtist( artistaId )
          .subscribe( (artista: any) => {
            this.artista = artista;            
            this.loading = false;
          });
  }
  getArtistTopTracks( artistaId: string ) {
    this.spotifyService.getArtistTopTracks(artistaId)
          .subscribe( (tracks: any) => {
              this.tracks = tracks;
          });
  }
}
